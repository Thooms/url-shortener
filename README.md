This is a toy URL shortener. It is not supposed to be efficient or ready to scale.

# Usage

* You will have to have `stack` installed
* To build, just checkout the repository, and `stack build`
* Then `./urlshort -p 3000 -d http://localhost -b yolo.sqlite3`
* Go to `http://localhost:3000` with a browser

# Documentation

Use `hyakko`: `hyakko src/*`

# Benchmarking/loadtest

Let's use `locust`! Just issue `locust --host=http://localhost:3000 -f test/bench.py`
and head over to <http://127.0.0.1:8089/>.
