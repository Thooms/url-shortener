#coding: utf8

import random
import urllib
import re
import threading

from locust import HttpLocust, TaskSet, task

REG = re.compile('">([/:\.\w]*)</a>')
LCK = threading.Lock()

class RegisterBehavior(TaskSet):

    def random_url(self):
        return urllib.quote('http://foobar.com/{}'.format(
            ''.join(map(str, random.sample(xrange(100), 10)))),
            safe=''
        )
    
    @task
    def register(self):
        url = '/?URL={}'.format(self.random_url())
        with self.client.get(url, name='/?URL=[url]', catch_response=True) as response:
            if REG.search(response.content):
                short = REG.search(response.content).group(1)
                with LCK:
                    self.parent.urls.append('/' + short.split('/')[-1])
                response.success()
            else:
                response.failure('Unable to register URL')        

class RetrieveBehavior(TaskSet):

    tasks = {RegisterBehavior: 10}

    urls = []
    
    @task(100)
    def retrieve(self):
        if not self.urls:
            return
        with LCK:
            url = random.choice(self.urls)
        with self.client.get(url, name='/[short-url]', catch_response=True) as response:
            if response.status_code == 301:
                response.success()
            else:
                response.failure('Unable to retrieve redirection')

class WebSiteUser(HttpLocust):
    task_set = RetrieveBehavior
