
-- Do you know bit.ly? It's a webservice called a *URL shortener*.
-- It means that you feed it with a long URL, and it returns back
-- some kind of ID, which you can use to share easily your long URL
-- to internet fellas.
--
-- Now, here we implement one. The idea is simple: it should work,
-- and be readable and short, that's why we use Haskell.
-- The design choices are simple too:
--
-- * Usage of `sqlite`, because keep it simple
-- * Let's not reinvent the wheel
-- * Early-prototype philosophy: let's have it work first, then
--   tackle the unavoidable problems.
--
-- This source article is generated from the source, and is
-- supposed to be readable by any programmer that have an idea
-- of what is an HTTP request.

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

import Blaze.ByteString.Builder (copyByteString)
import Control.Applicative
import qualified Control.Exception as EX
import Control.Monad
import qualified Data.ByteString.UTF8 as BU
import Data.Maybe
import Data.Monoid
import Data.Text.Encoding (encodeUtf8)
import Data.Time.Clock
import qualified Database.SQLite.Simple as SQ
import Database.SQLite.Simple.FromRow
import Network.HTTP.Types (status200, status404, status301)
-- We use Warp, which is the most minimal HTTP server you can find
-- in Haskell.
import Network.Wai
import Network.Wai.Handler.Warp
import Numeric (showHex)
import Prelude hiding (log)
import System.Console.GetOpt
import System.Directory (doesFileExist)
import System.Environment (getArgs)
import System.IO (FilePath, openFile, hClose, IOMode(..))
import Data.Digest.Murmur64 (hash64, asWord64)
import Numeric (showHex)

-- The overall architecture is quite simple:
-- 
-- * Open a DB connection, setup the DB as required
--   (a table `urls` wich contains `(id, slug, url)`), a slug being
--   a unique identifier that will map to an URL
-- * run the Warp serv
-- * for each incoming request, dispatch to the appropriate handler
-- * each handler will craft the HTTP anwser

buildUrl :: Env -> BU.ByteString -> BU.ByteString
buildUrl env endpoint =
  rootDomain env <> ":" <> (BU.fromString . show $ port env)
  <> "/" <> endpoint

-- # Slug generation

-- The main slug generation function. The principle is simple:
-- we use a hash (the 64 bits Murmur hash here) to have the slug generated
-- in a deterministic manner, and we return it in base 16.

getSlug :: BU.ByteString -> BU.ByteString
getSlug url = BU.fromString $ (showHex . toInteger . asWord64 . hash64 $ url) ""

-- # Logging

-- A production-like logger is overkill here,
-- so we just manage three levels of log, and for now print
-- everything that's not an INFO on STDOUT.

data LogLevel = Info | Debug | Error deriving Eq

log :: LogLevel -> String -> IO ()
log lvl msg = do
  t <- getCurrentTime
  putStrLn $ "[" ++ show t ++ "] " ++ flag lvl ++ " " ++ msg
  where flag Info = "[Info]"
        flag Debug = "[Debug]"
        flag Error = "[Error]"

logRequest :: Request -> Int -> IO ()
logRequest req code =
  log Info $ show code ++ " " ++ show (remoteHost req)
                       ++ " " ++ show (rawPathInfo req)
                       ++ " " ++ show (rawQueryString req)

-- # Database management

-- The database stuff in this project is fairly minimal
-- (one table, no real optimization for now),
-- so we use a simple driver (`sqlite-simple`) to issue
-- our SQL queries.

-- This will return a handle to a SQLite database at the
-- given path, and if does not exists, will create it.

getOrOpenDB :: FilePath -> IO SQ.Connection
getOrOpenDB path = do
  exists <- doesFileExist path
  if exists then SQ.open path else do
    openFile path ReadWriteMode >>= hClose -- equivalent to a UNIX `touch`
    conn <- SQ.open path
    setupDB conn
    return conn

-- We define a convenient wrapper that will take care of
-- opening and closing the database connection whenever
-- it's relevant (in case of an IOException for instance).
-- It also logs if an exception happens.

withOpenDB :: FilePath -> (SQ.Connection -> IO ()) -> IO ()
withOpenDB path action =
  (EX.bracket (getOrOpenDB path) SQ.close action)
  `EX.catch` (\(e :: EX.SomeException) -> log Error (show e))

-- SQL queries, quite self-explainatory.

setupDB :: SQ.Connection -> IO ()
setupDB conn =
  SQ.execute_ conn
  "CREATE TABLE IF NOT EXISTS urls (id INTEGER PRIMARY KEY, short TEXT UNIQUE, target_url TEXT)"

createRedirection :: SQ.Connection -> BU.ByteString -> BU.ByteString -> IO ()
createRedirection conn slug url =
  SQ.execute conn
  "INSERT INTO urls (short, target_url) VALUES (?, ?)"
  (BU.toString slug, BU.toString url)

getRedirection :: SQ.Connection -> BU.ByteString -> IO (Maybe BU.ByteString)
getRedirection conn slug = do
  res <- SQ.query conn "SELECT target_url FROM urls WHERE short = (?)" (SQ.Only $ BU.toString slug)
  case res of
    [] -> return Nothing
    ((SQ.Only (url :: String)):_) -> return $ Just $ BU.fromString url

getSlugFromUrl :: SQ.Connection -> BU.ByteString -> IO (Maybe BU.ByteString)
getSlugFromUrl conn url = do
  res <- SQ.query conn "SELECT short FROM urls WHERE target_url = (?)" (SQ.Only $ BU.toString url)
  case res of
    [] -> return Nothing
    ((SQ.Only (slug :: String)):_) -> return $ Just $ BU.fromString slug

-- Here is the main URL registering logic:
--
-- * We first lookup for a equal URL that's already registered
-- * If yes, we return the corresponding slug
-- * If no, we hash the URL, and that gives us the slug for it

registerNewUrl :: SQ.Connection -> BU.ByteString -> IO BU.ByteString
registerNewUrl conn = trySlug
  where trySlug url = do
          slug_ <- getSlugFromUrl conn url
          case slug_ of
            Just slug -> return slug -- let's reuse it!
            Nothing -> do
              let newSlug = getSlug url
              createRedirection conn newSlug url
              return newSlug

-- # Web part

-- The following code is heavily inspired from Yesod's model:
-- A request is first parsed, and then a handler is chosen to
-- return something from the request.
-- All the logic is written in the handlers.

-- A `Handler` is a response to a request, with knowledge of the
-- global shared environment.

type Handler = Env -> Request -> IO Response

-- This is what will route a request to the appriopriate piece of code.
-- The internal function `respondWithLogging` is simply a wrapper that
-- will log the incoming connection, and catch any exception that could
-- happen before responding a 404.
-- We do not have outstanding needs in term of URL parsing, so we just
-- pattern-match on the URL components.

dispatcher :: Env -> Application
dispatcher env req respond = respondWithLogging respond req $ case pathInfo req of
  [] -> homeR
  [endpoint] -> redirectionR
  ["api", "stats"] -> statsR
  _ -> failR

  where respondWithLogging :: (Response -> IO ResponseReceived)
                           -> Request
                           -> Handler
                           -> IO ResponseReceived
        respondWithLogging respond req handler =
          (respond =<< handler env req)
          `EX.catch`
          (\(e :: EX.SomeException) -> log Error (show e) >> (respond =<< failR env req))

-- The mainpage handler. It accepts two URL patterns:
--
-- * `/`: just print the homepage
-- * `/?URL=someurl.html` (encoded correctly, of course): register the URL given in parameter
--   and render the apprioriate page (result or fail)
--
-- Note that the page rendering is quite basic: just a list of strings
-- with the right variables at the right places, passed to a build that will
-- format the HTTP answer correctly.

homeR :: Handler
homeR env req = case queryString req of
  [] -> logRequest req 200 >> homePage
  [("URL", Just url)] -> logRequest req 200 >> reg url
  _ -> failR env req

  where conn = dbConn env
        buildPage = return
                    . responseBuilder status200 [("Content-Type", "text/html")]
                    . mconcat . map copyByteString
        homePage =
          buildPage
          $ [ "<html><body><p>Hello, please register your URL.</p>"
            , "<form>"
            , "<input type=\"text\" name=\"URL\" />"
            , "<input type=\"submit\" value=\"submit\" />"
            , "</form>"
            , "</body></html>"
            ]
        reg url = do
          slug <- registerNewUrl conn url
          let finalUrl = buildUrl env slug
          buildPage
            $ [ "<html><body>"
              , "<p><a href=\"", finalUrl ,"\">", finalUrl, "</a></p>"
              , "</body></html>"
              ]

-- A simple 404.

failR :: Handler
failR _ req = do
  logRequest req 404
  return $ responseBuilder status404 [("Content-Type", "text/html")]
    $ copyByteString
    $ "<html><body><p>DA FAIL</p></body></html>"

-- This one is a bit more interesting. We first lookup the slug in the database,
-- and fails if it doesn't exists. If it does, we answer a 301 HTTP code
-- (Permanent redirection).

redirectionR :: Handler
redirectionR env req = do
  urlResult <- getRedirection (dbConn env) (encodeUtf8 . head . pathInfo $ req)
  case urlResult of
    Nothing -> failR env req
    Just url -> do
      logRequest req 301
      return $ responseBuilder status301 [("Content-Type", "text/html"), ("Location", url)]
        $ mconcat . map copyByteString
        $ [ "<html><body>"
          , "<p>Get <a href=\"", url,"\">there<a/></p>"
          , "</body></html>"
          ]

statsR :: Handler
statsR env req = do
  queryOutput <- SQ.query_ (dbConn env) "SELECT COUNT(short) FROM urls"
  let res = case queryOutput of
        [] -> 0
        ((SQ.Only x):_) -> (x :: Int)
  logRequest req 200
  return $ responseBuilder status404 [("Content-Type", "text/html")]
    $ mconcat . map copyByteString
    $ [ "<html><body><p>"
      , BU.fromString (show res)
      , " URL shortened, yay!</p></body></html>"
      ]

-- # Server logic

-- Here we treat all the CLI and Warp-related (Warp is the server we use) stuff.
-- So first, we define a type of Environment that all the requests handlers will
-- receive. It contains everything needed to answer a request statelessly.

data Env = Env { dbConn :: SQ.Connection
               , dbPath :: FilePath
               , rootDomain :: BU.ByteString
               , port :: Int
               , debugMode :: Bool
               }

instance Show Env where
  show env = "<"
             ++ dbPath env ++ ", "
             ++ show (rootDomain env) ++ ":" ++ show (port env)
             ++ (if debugMode env then "[DEBUG]" else "")
             ++  ">"

defaultEnv :: Env
defaultEnv = Env { dbConn = undefined
                 , dbPath = "yolo.sqlite3"
                 , rootDomain = "localhost"
                 , port = 3000
                 , debugMode = False
                 }

-- Some option parsing, very basic.

options :: [OptDescr (Env -> Env)]
options =
  [ Option ['p'] ["port"]
    (ReqArg (\p env -> env {port = read p}) "PORT")
    "Specify the port on which listen to connections."

  , Option ['d'] ["domain"]
    (ReqArg (\d env -> env {rootDomain = BU.fromString d}) "http://MYDOMAIN.COM/")
    "Specify the root domain of the application (don't forget the trailing slash)."

  , Option ['b'] ["database"]
    (ReqArg (\path env -> env {dbPath = path}) "yoloswag.sqlite3")
    "Specify the database filepath."

  , Option ['t'] ["debug"]
    (NoArg (\env -> env {debugMode = True}))
    "Turn on debug mode."
  ]

parseArgs :: [String] -> IO (Env, [String])
parseArgs argv = case getOpt Permute options argv of
  (opts, _, []) -> do
    when (length opts < 3) fail 
    return $ (foldl (flip id) defaultEnv opts, [])
  _ -> fail
  where header = "Usage: ./urlshort [OPTION...]"
        fail = ioError . userError $ usageInfo header options

-- And the server entrypoint :)

main :: IO ()
main = do
  args <- getArgs
  (env, _) <- parseArgs args
  log Info $ "Listening on port " ++ show (port env)
  when (debugMode env) $ log Debug "Debug mode activated"
  withOpenDB (dbPath env) $ \conn -> do
    when (debugMode env) $ conn `SQ.setTrace` (Just (log Debug . show))
    run (port env) $ dispatcher $ env {dbConn = conn}

-- # Conclusion

-- Ok, so we have a working URL shortener in less than 400 LOC. That's great.
-- What did we learned?
